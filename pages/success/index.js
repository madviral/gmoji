import Head from "next/head";
import Image from "next/image";
import styles from "../../styles/Common.module.css";
import gift from "../../public/images/gift.png";

export default function Success() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Gmoji</title>
        <meta name="description" content="Gmoji" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.titleSecondary}>Поздравляем</h1>
        <h1 className={styles.titleSecondary}>Ваш подарок 5 000₸</h1>
        <p className={styles.descriptionSecondary}>Деньги поступят на баланс вашего номера в течении 3 минут</p>
        <Image alt="gift" src={gift} layout="intrinsic" width={300} height={154} />
      </main>
    </div>
  );
}

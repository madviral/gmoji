import Head from "next/head";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import InputMask from "react-input-mask";

import styles from "../styles/Common.module.css";

export default function Home() {
  const router = useRouter();
  const { pin } = router.query;
  const [phone, setPhone] = useState("");
  const [code, setCode] = useState("");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const obj = router.query;
    for (const prop in obj) {
      if (obj.hasOwnProperty(prop)) {
        if (prop.includes("entry")) {
          setCode(obj[prop]);
        }
      }
    }
  }, [router.query]);

  const onPhoneChange = (value) => {
    const newVal = value.replace(/\s/g, "").replace(/\(/g, "").replace(/\)/g, "").substring(1);
    setPhone(newVal);
  };

  const onBtnClick = () => {
    if (pin && code) {
      if (phone.length === 11) {
        setLoading(true);
        const requestOptions = {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({ phone, pin, code }),
        };
        fetch("gifts/api/promo/send-gift", requestOptions).then(
          (res) => {
            setLoading(true);
            if (res.status === 200) {
              router.push("/success");
            } else {
              alert("Купон уже активирован!");
            }
          },
          () => {
            setLoading(false);
            alert("Сервер не доступен,попробуйте позже");
          }
        );
      } else {
        alert("Заполните номер телефона");
      }
    } else {
      alert("Параметры pin и code отсутствуют");
    }
  };

  return (
    <div className={styles.container}>
      <Head>
        <title>Gmoji</title>
        <meta name="description" content="Gmoji" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>Активация подарка</h1>
        <p className={styles.description}>Для активации подарка просто введите номер телефона</p>
        <div className={styles.grid}>
          <InputMask
            type="tel"
            className={styles.field}
            mask="+7(999) 999 99 99"
            alwaysShowMask={true}
            onChange={(e) => onPhoneChange(e.target.value)}
          />
          <button className={styles.btn} onClick={() => onBtnClick()}>
            {loading ? "Загрузка..." : "Отправить"}
          </button>
        </div>
      </main>
    </div>
  );
}
